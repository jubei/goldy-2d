var helpers = require('./helpers');

var webpack = require('webpack');

var HtmlWebpackPlugin = require('html-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');


module.exports = {
    mode: 'production',
    entry: helpers.root('src') + "/index.tsx",
    output: {
        filename: "bundle.js",
        path: helpers.root('public')
    },

    // Enable sourcemaps for debugging webpack's output.
    devtool: "source-map",

    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: [".ts", ".tsx", ".js", ".json"]
    },

    plugins: [
    new CopyWebpackPlugin([
        { from: helpers.root('src') + '/assets/', to: 'assets'},
        { from: helpers.root('node_modules') + '/react/umd/react.production.min.js', to: 'libs/react.js'},
        { from: helpers.root('node_modules') + '/react-dom/umd/react-dom.production.min.js', to: 'libs/react-dom.js'},
  ]),
    new HtmlWebpackPlugin({
            title: 'Goldy – bring money where it belongs!',
            template: helpers.root() + '/main.html',
            favicon: helpers.root() +'/favicon.ico',
            filename: helpers.root('public') + '/index.html',
    })],
    module: {
        rules: [
            // All files with a '.ts' or '.tsx' extension will be handled by 'awesome-typescript-loader'.
            { test: /\.tsx?$/, loader: "awesome-typescript-loader" },
            {
                test: /\.scss$/,
                use: [{
                    loader: "style-loader" // creates style nodes from JS strings
                }, {
                    loader: "css-loader" // translates CSS into CommonJS
                }, {
                    loader: "sass-loader" // compiles Sass to CSS
                }]
            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                loader: 'file-loader',
                options: {
                  name:  'assets/images/[name].[ext]?[hash]',
                }
              },
            // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
            { enforce: "pre", test: /\.js$/, loader: "source-map-loader" }
        ]
    },

    // When importing a module whose path matches one of the following, just
    // assume a corresponding global variable exists and use that instead.
    // This is important because it allows us to avoid bundling all of our
    // dependencies, which allows browsers to cache those libraries between builds.
    externals: {
        "react": "React",
        "react-dom": "ReactDOM"
    },
};

module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    })
]);