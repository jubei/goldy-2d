# Goldy 2D

Navigate board and deliver goods. Development of this game was inspired by SOKOBAN Game on ESTERA28 mobile phone (ref.: https://youtu.be/NSu87Wfb_3Y)

## Game is live

Visit: https://jubei.gitlab.io/goldy-2d

## Developer guide

- download this repo
- install dependencies with `npm install`
- administrators version has level manager `npm start`
- users version `npm run build` now see `public` dir

## Contribution

Please clone this repository and start your work with development branch. Create a new branch for any features you add.


## Updates: 

- Updating packages and introducing new terms (n, m, i, j). Removing x, y (2018 05-19)
- Improving Level Manager and adding 3 new Levels (2017 12-12)
- Adding Level Manager for Admin (2017 12-11)
- Adding NEW levels and audio effects (2017 12-08)