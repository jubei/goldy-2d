/* This updates ../src/components/Engine/levels.json */
const fs = require('fs');
const qs = require('querystring');

const formidable = require('formidable');
const helpers = require('../../config/helpers');

const publishTo = '/components/Engine/levels.json';
var LEVELS = require(helpers.root('src')+publishTo);
var message = "Nothing here";

function updateLevels(req, cb) {
    let parts = req.url.split('/');

    var form = new formidable.IncomingForm();
    var publish = false;  


    return form.parse(req, function(err, fields, files){
        if (err) throw new Error(err.message);

        switch (req.method.toLowerCase()) {
            case "post": //creating a new
                message = "OK";
                LEVELS = fields.levels;
                break;
            break;
        }
        
        fs.writeFile(helpers.root('src')+publishTo, JSON.stringify(LEVELS), { flag : 'w' }, function(err) {
            if (err) {
                cb(err.message);
            } else {
                cb(message);
            }   
        });
    });    
}

module.exports = updateLevels;