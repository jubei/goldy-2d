import * as React from "react";
import * as ReactDOM from "react-dom";

import { Game } from "./components/Game/game";
import { GameAdmin } from "./components/GameAdmin/game-admin";

import './main.scss';

const throttle = 500;
var lastKayboardEvent = +new Date();

function start() {
    update();
}

function adjustBoardHeight(){
    if (window.innerWidth <= 640) {

        let squares = document.querySelectorAll('.square');
        let newSquareHeight = window.innerWidth / 9;

        for (let n = 0; n < squares.length; n++) {
            squares[n].setAttribute('style', "height:"+newSquareHeight + 'px;'); 
        }
    }    
}

function update(action : string  = null) {
    if (action && +new Date() - lastKayboardEvent < throttle) {
        action = undefined;
    }

    if (process.env.hasOwnProperty('NODE_ENV')) {
        if (process.env['NODE_ENV'] === 'development') {
            ReactDOM.render(        
                <div>
                    <div id="gameArea">
                        <Game adjustBoardHeight={adjustBoardHeight} action={action} />
                    </div>
                    <div id="adminArea">
                        <GameAdmin />
                    </div>
                </div>
                ,
                document.getElementById("app")
            );
        } else {
            ReactDOM.render(        
                <div>
                    <Game adjustBoardHeight={adjustBoardHeight} action={action} />
                </div>
                ,
                document.getElementById("app")
            );
        }
    }

  
}


window.addEventListener('load', function(){
    start();

    document.addEventListener('keydown', function(e: any) {
        switch(e.key) {
            case 'Left': 
                update('Arrow'+e.key);
                break;
            case 'ArrowLeft': 
                update(e.key);
                break;
            case 'Right':
                update('Arrow'+e.key);
                break;
            case 'ArrowRight':
                update(e.key);
                break;
            case 'Up':
                update('Arrow'+e.key);
                break;
            case 'ArrowUp':
                update(e.key);
                break;
            case 'Down':
                update('Arrow'+e.key);
                break;
            case 'ArrowDown':
                update(e.key);
                break;    
            default: return;
        }

        e.preventDefault();
    });
});

