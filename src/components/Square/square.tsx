import * as React from "react";
import { Row } from "../Row/row";

export interface SquareProps {
  onClick: any,
  row : number,
  col : number,
  value: string
}

export class Square extends React.Component<SquareProps, {}> {

  render(){
    //onDragStart={} onDragEnd={} 
    return (
      <button ref="self" onClick={()=>this.props.onClick(this.props.row, this.props.col, this.refs.self)}  className={"square s"+this.props.row+"-"+this.props.col} dangerouslySetInnerHTML={{__html: this.props.value}}></button>
    );
  }

}