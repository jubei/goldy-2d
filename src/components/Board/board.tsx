import * as React from "react";

import { Row } from '../Row/row';

export interface BoardProps { squares: any[]; rows: number; cols: number; onClick: any; }

export class Board extends React.Component<BoardProps, {}> {
    render() {
        const rows : any[] = [];

        for (let i = 0; i < this.props.rows; i++){
            let key = "r" + i;
            let squares = this.props.squares.slice(i*this.props.cols, i*this.props.cols+this.props.cols)
            rows.push(<Row onClick={this.props.onClick} cols={this.props.cols} row={i} key={key} squares={squares} />);
        }

        return (
            <div className="board">
                {rows}
            </div>
        );
    }
}