import * as React from "react";

export interface AudioEffectProps { src: string; id: string; play ?: boolean }

export class AudioEffect extends React.Component<AudioEffectProps, {}> {
  element: any;

  componentDidMount(){
   this.componentDidUpdate();
  }

  componentDidUpdate(){
    if (this.props.play){
      let audio : any = document.getElementById(this.props.id);
      audio.play();
    }
  }

  render() {
    let element = React.createElement("audio",
      { id: this.props.id },
      <source src={this.props.src}></source>,
    );


    this.element = element;

    return this.element
  }
}