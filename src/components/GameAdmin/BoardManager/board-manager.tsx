import * as React from 'react';

import { Board } from '../../Board/board';
import { GameBoard } from '../../Engine/GameBoard/game-board';
import { Cell } from '../../Engine/Cell/cell';
import { gridToEmojiGrid } from '../../Engine/emojis';
import { ControlGroup } from '../ControlGroup/control-group';

export interface BoardManagerProps {
    board: GameBoard;
    onSave: Function;
    onModify: Function;
    onDelete: Function;
    levelID: number;
    moveLeft: Function;
    moveRight: Function;
}

export interface BoardManagerState {
    grid: Cell[];
}

export class BoardManager extends React.Component<BoardManagerProps, BoardManagerState> {
    que: any = null;
    grid: Cell[];

    constructor(props : any){
        super(props);

        this.clickHandler = this.clickHandler.bind(this);

        this.grid = this.props.board.grid;

        this.state = {
            grid: this.props.board.grid
        }

        this.onClickSave = this.onClickSave.bind(this);
        this.onDelete = this.onDelete.bind(this);
    }

    clickHandler(row: number, col: number, cell: any) {
        this.props.onModify(this.props.levelID);
        
        if (this.que !== null) {
            this.que.cell.style.backgroundColor = "#ebebeb";
            let prevPos = this.props.board.getPos(this.que.row, this.que.col);
            let currPos = this.props.board.getPos(row, col);
            this.grid[prevPos].storage.type = this.grid[currPos].storage.type;
            this.flushState();
            this.que = null;
        } else {
            cell.style.backgroundColor = "#f9ffc3";

            this.que = {
                row: row,
                col: col,
                cell
            };
        }
    }

    flushState() {
        this.setState({grid: this.grid});
    }


     componentWillReceiveProps(props: any) {
        this.grid = props.board.grid;

         this.setState({
             grid: props.board.grid
        });
     }

     onClickSave() {
        this.props.onSave(this.state.grid);
     }

     onDelete() {
        this.props.onDelete(this.props.levelID);
     }

    render() {
        return (
            <div>
                {/* <div><button onClick={this.saveConfiguration}>Save</button> <button onClick={this.createNew}>New Level</button></div> */}
                <Board onClick={this.clickHandler} squares={gridToEmojiGrid(this.state.grid)} rows={this.props.board.rows} cols={this.props.board.cols}  />
                <ControlGroup moveLeft={this.props.moveLeft} moveRight={this.props.moveRight} onClickDel={this.onDelete} onClickSave={this.onClickSave} saved={false} />
            </div>
        )
    }
}