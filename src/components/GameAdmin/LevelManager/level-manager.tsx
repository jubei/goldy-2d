import * as React from 'react';
import axios from 'axios';


import { GameLevelStructure, GameLevel } from '../../Engine/GameLevel/game-level';

import './level-manager.scss';
import { LevelSelector } from './level-selector';
import { BoardManager } from '../BoardManager/board-manager';
import { GameBoard } from '../../Engine/GameBoard/game-board';
import { gameBoardFromTemplate } from '../../Engine/GameBoard/game-board-from-template';
import { Template } from '../../Engine/Template/template';
import { Cell } from '../../Engine/Cell/cell';
import { GameEntityType } from '../../Engine/GameEntity/game-entity';
import { ImportantMessage } from '../ImportantMessage/important-message';
import { TIMEOUT } from 'dns';

export interface LevelManagerProps {
    levels: GameLevelStructure[]
}

export interface LevelManagerState {
    levelSelected: number;
    board: GameBoard;
    message: string;
    modified: number[];
    saved: number[];
}

export class LevelManager extends React.Component<LevelManagerProps, LevelManagerState> {
    mystate: LevelManagerState;
    levels: GameLevel[];

    constructor(props : any) {
        super(props);

        if (this.props.levels.length){
            this.levels = this.props.levels.map(template=>new GameLevel(new Template(template)));

            this.mystate = {
                levelSelected: 0,
                board: null,
                message: null,
                modified: [],
                saved: []
            }

            this.select(0);
        } else {

            this.mystate = {
                levelSelected: null,
                board: null,
                message: null,
                modified: [],
                saved: []
            }
        }

        this.state = this.mystate;
        
        this.select = this.select.bind(this);
        this.save = this.save.bind(this);
        this.modify = this.modify.bind(this);
        this.create = this.create.bind(this);
        this.delete = this.delete.bind(this);
        this.publish = this.publish.bind(this);

        this.moveLeft = this.moveLeft.bind(this);
        this.moveRight = this.moveRight.bind(this);

        this.resetMessage = this.resetMessage.bind(this);
    }

    modify(n : number) {
        let modified = this.state.modified;
        let saved = this.state.saved;

        delete saved[saved.indexOf(n)];

        if (modified.indexOf(n) == -1) {
            modified.push(n);
        }

        this.mystate.modified = modified;
        this.mystate.saved = saved;

        this.setState(this.mystate);
    }
    
    select(n : number) {
        if (n < 0 && n > this.props.levels.length) throw new Error('Level does not exist');

        this.mystate.levelSelected = n;
        this.mystate.board = gameBoardFromTemplate(new Template(this.levels[n].template));

        this.setState(this.mystate);
    }

    publish() {
        let promise = axios.post('/api/levels', { levels: this.levels.reduce((acc, level)=>{ acc[acc.length] = level.template; return acc }, []) });

        promise.then(
            success=>{
                this.mystate.message = "Published.";
                this.mystate.saved = [];
                this.setState(this.mystate);
            },
            errorMessage=>{
                throw new Error(errorMessage);
            }
        )
    }

    moveLeft() {
        if (this.state.levelSelected >= 1) {
            let copy = JSON.stringify(this.levels[this.state.levelSelected-1].template);
            this.levels[this.state.levelSelected-1] = this.levels[this.state.levelSelected];
            this.levels[this.state.levelSelected] = new GameLevel(new Template(JSON.parse(copy)));
            this.select(this.state.levelSelected-1);
        }
    }


    moveRight() {
        if (this.state.levelSelected < this.levels.length-1) {
            let copy = JSON.stringify(this.levels[this.state.levelSelected+1].template);
            this.levels[this.state.levelSelected+1] = this.levels[this.state.levelSelected];
            this.levels[this.state.levelSelected] =   new GameLevel(new Template(JSON.parse(copy)));
            this.select(this.state.levelSelected+1);
        }     
    }


    save(board: Cell[]) {
        let modified = this.state.modified;
        let saved = this.state.saved;

        delete modified[modified.indexOf(this.state.levelSelected)];
        saved.push(this.state.levelSelected);

        var template : GameLevelStructure = { layout: "", breakpoint: Math.sqrt(board.length) };
        board.forEach((cell: Cell)=>{
            template.layout += GameEntityType[cell.storage.type][0];
        });

        this.levels[this.state.levelSelected] = new GameLevel(new Template(template));

        this.mystate.message = "Saved";
        this.mystate.saved =  saved;
        this.mystate.modified = modified;

        this.setState(this.mystate);
    }

    create() {        
        this.levels.push(new GameLevel(new Template({layout:'GPODEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE', breakpoint: 9})));
        this.select(this.levels.length-1);
    }

    delete() {
        if (this.levels.length > 1) {
            this.levels = [].concat(this.levels.slice(0, this.mystate.levelSelected), this.levels.slice(this.mystate.levelSelected+1));
        }

        this.mystate.message = "Removed.";
        this.select(0);        
    }

    componentDidUpdate() {
        this.mystate.message = undefined;
    }

    resetMessage() {
        this.mystate.message = null;
    }

    render() {
        if (this.state.levelSelected !== undefined) {
            return (
                <div id="level-manager">
                    <div className="clearfix level-selector">
                        <ImportantMessage resetMessage={this.resetMessage} text={this.state.message}  />
                    </div>
                    <div className="clearfix"> <button className="strech" onClick={this.create}>New</button></div>
                    <div className="clearfix"> <button className="strech" onClick={()=>this.publish()}>Publish Changes</button></div>
                    <div className="clearfix level-selector">
                        <LevelSelector total={this.levels.length} current={this.state.levelSelected}  onClick={this.select} saved={this.state.saved} modified={this.state.modified} />
                    </div>
                    <div className="board-manager">
                        <BoardManager moveLeft={this.moveLeft} moveRight={this.moveRight}  levelID={this.state.levelSelected} onDelete={this.delete} onSave={this.save} board={this.levels[this.state.levelSelected].board} onModify={this.modify} />
                    </div>

                    
                </div>
            )
        } else {
            return (
                <div id="level-manager">
                  <div id="level-selector">
                    <LevelSelector moveLeft={this.moveLeft} moveRight={this.moveRight} total={this.levels.length} current={this.state.levelSelected} onClick={this.select} />
                    </div>
                </div>
            )
        }
    }
}

