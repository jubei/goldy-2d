import * as React from 'react';

export function LevelSelector(props: any) {
    var listElements : any = [];
    
    for (let index = 0; index < props.total; index++) {
        let m = "", s = "";
        if (props.modified.indexOf(index) > -1) m = "modified";
        if (props.saved.indexOf(index) > -1) s = "saved";

        if (props.current === index) {
            listElements.push(<li onClick={()=>props.onClick(index)} key={index} className={['active', m, s].join(" ")}><a href="#">L{index}</a></li>);
        } else {
            let classList = [m, s].reduce((acc, cv)=>acc+cv+" ").trim();
            listElements.push(<li onClick={()=>props.onClick(index)} className={[m, s].join(" ")} key={index}><a href="#">L{index}</a></li>);
        }
    }

    return (<ul>{listElements}</ul>);
}