import * as React from 'react';
import './important-message.scss';

export interface ImpoartantMessageProps {
    text: string;
    resetMessage: Function;
}

export class ImportantMessage extends React.Component<ImpoartantMessageProps,{}> {
    id: number = 0;
    ref: any;

    componentDidMount() {
        this.hide();
    }

    componentDidUpdate() {
        let el = document.getElementById('important-message');
        if (el) {
            if (el.style.display === 'none') {
                el.style.display = 'block';
            }
            this.hide();
        }
    }

    hide() {
        setTimeout(()=>{
            document.getElementById('important-message').style.display = 'none';
            this.props.resetMessage();
        }, 3000);
    }

    render() {
        if (this.props.text === undefined || this.props.text === null || this.props.text.length === 0) {
            return (<span></span>);
        }

        this.id = Math.ceil(Math.random()*90000)+10000;    
        return (<p className="message" id={'important-message'}>{this.props.text}</p>)
    }
}