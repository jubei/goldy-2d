import * as React from "react";

import { Engine } from "../Engine/engine";
import { LevelManager } from "./LevelManager/level-manager";

import './game-admin.scss';

const LEVELS = require('../Engine/levels.json');

export class GameAdmin extends React.Component<{}, {}> {
    engine: Engine = undefined;

    constructor(props: any) {
        super(props);        
    }

    loadLevels() {
        return LEVELS;
    }

    render() {    
        return (  
            <div id="admin-area">
                <h2>Goldy Admin</h2>
                <p>Modify levels to your liking.</p>
                <LevelManager levels={this.loadLevels()} />
            </div>
        )
    }
}