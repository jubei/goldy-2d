import * as React from 'react';

export function ControlGroup(props: any) {
    if (props.saved) {
        return (
            <div>
                <button disabled>Save</button> 
                <button onClick={()=>{props.onClickDelete(props.level)}}>Delete</button> 
                <button onClick={()=>props.moveLeft()}>Move Left</button> 
                <button onClick={()=>props.moveRight()}>Move Right</button>
            </div>
        )
    } else {
        return (
            <div>
                <button onClick={()=>{props.onClickSave()}}>Save</button> 
                <button onClick={()=>{props.onClickDel()}}>Delete</button> 
                <button onClick={()=>{props.moveLeft()}}>Move Left</button> 
                <button onClick={()=>{props.moveRight()}}>Move Right</button> 
            </div>
        )
    }
}