import { Entity } from "../Entity/entity";
import { Board } from "../Board/board";

export class BoardEntity extends Entity {
    constructor(
        type : number,
        protected row: number,
        protected col: number,
        protected board: Board
    ) {
        super(type, board.getPos(row, col));
    }
}

export default BoardEntity;