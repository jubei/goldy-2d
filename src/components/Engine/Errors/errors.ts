export enum GenericError {
    TooFewArguments = "Too few arguments provided."
}

export enum CellError {
    AcceptsOnlyInstancesOfGameEntity = "Cell stores instances GameEntity only."
}

export enum GameError {
    LevelDoesNotExist = "Level does not exist.",
    InitiateGameBoardFirst = "Initiate game board first.",
    Unsolvable = "Game unsolvable.",
    PlayerMissing = "Player missing."
}

export enum GameBoardError {
    InvalidBound = "Invalid coordinates.",
    OutOfSpace = "Board is out of space.",
    NaturalNumbers = "Only natural numbers accepted."
}

export enum EntityError {
    UndefinedEntityType = "Undefined entity type."
}

export enum TemplateError {
    InvalidBase = "Invalid base number.",
    IllegalCharactersFound = "Template contains illegal characters.",
    TemplateStringEmpty = "Template string empty.",
    TemplateDimentionsError = "Breakpoint and template string do not match."
}

export enum GameEntityError {
    IllegalCoordinates = "Illegal coordinates.",
    EntityOffline = "Entity offline."
}

export default GenericError;