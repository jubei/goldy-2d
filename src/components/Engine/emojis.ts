import { Cell } from "./Cell/cell";
import { GameEntityType } from "./GameEntity/game-entity";

export const EMOJIS : any = {
    "P":"🦊", /* P for Player */
    "G":"💲", /* G for Gold */
    "O": "🛑", /* O for Obsticle */
    "D": "<img src='./assets/pyramid.gif'/>", /* D for Destination */
    "E": "", "C": "<img src='./assets/pyramid-green-64px.gif'/>" /* E for Empty */
};


export function gridToEmojiGrid(grid : any) {
    let squares : any = [];

   grid.forEach((cell : Cell) => {
        let c = GameEntityType[cell.storage.type][0];
        if (cell.storage.type === GameEntityType.Gold && cell.getPocket() !== undefined) {
            c = "C";       
        }
        squares.push(EMOJIS[c]);
    });

    return squares;
}