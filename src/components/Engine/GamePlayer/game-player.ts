import { GameEntity, GameEntityType } from "../GameEntity/game-entity";
import { GameBoard } from "../GameBoard/game-board";
import { Cell } from "../Cell/cell";

export class GamePlayer {
   constructor(private gameBoard : GameBoard, public entity : GameEntity) {}

   up(){
       return this.move(0, -1);
   }

   left(){
        return this.move(-1, 0);
   }

   right(){
        return this.move(1, 0);
   }    

   down(){
        return this.move(0, 1);
   }

   move(horizontalVector : number = 0, verticalVector : number = 0) {
    let target = {
        i: this.entity.row+verticalVector,
        j: this.entity.col+horizontalVector
    }

        let targetPos = this.gameBoard.getPos(target.i, target.j);    
        let targetCell = this.gameBoard.grid[targetPos];
 
        let canStepOn = this.gameBoard.canStepOn(this.entity, targetCell);

        if (canStepOn){
            /* change target cell ? */
            return this.relocate(this.entity, targetCell);
        } else {
            let aheadPos = this.gameBoard.getPos(target.i+verticalVector, target.j+horizontalVector);
            let aheadCell : any = this.gameBoard.grid[aheadPos];
            let canStepOn = this.gameBoard.canStepOn(targetCell.storage, aheadCell);

            if (canStepOn) {
                this.relocate(targetCell.storage, aheadCell);
                this.move(horizontalVector, verticalVector);

                return true;
            }
        }

       return false;
   }

   relocate(e : GameEntity, targetCell : Cell) {
       this.gameBoard.relocate(e, targetCell);
   }
}

export default GamePlayer;