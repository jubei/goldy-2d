import { BoardEntity } from "../BoardEntity/board-entity";
import { GameBoard } from "../GameBoard/game-board";

export enum GameEntityType {
    Empty,
    Player,
    Gold,
    Obsticle,
    Destination
}

export class GameEntity extends BoardEntity {
    constructor(type: GameEntityType, public row : number = NaN, public col : number = NaN, gameBoard : GameBoard) {
        super(type, row, col, gameBoard);
    }

    get type() {
        return this.id;
    }

    set type(id: number) {
        this.id = id;
    }

    getPosition() {
        return this.pos;
    }
}

export default GameEntity;