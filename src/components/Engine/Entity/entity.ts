import { GenericError } from "../Errors/errors";

export class Entity {
    constructor(protected id: number, protected pos: number) {}
}

export default Entity;