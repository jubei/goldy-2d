import { Template } from '../Template/template';
import { GameBoard } from '../GameBoard/game-board';
import gameBoardFromTemplate from '../GameBoard/game-board-from-template';

export interface GameLevelStructure {
    layout: string;
    breakpoint: number;
}

export class GameLevel {
    public board : GameBoard;
    public template : Template;

    constructor(/*public */t : Template) {
       this.board =  gameBoardFromTemplate(t);
       this.template = t;
    }
}

export default GameLevel;