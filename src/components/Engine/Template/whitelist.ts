import { GameEntityType } from "../GameEntity/game-entity";

/* This produced an array containing E G P D O \n */
export const WHITELIST : string[] = [
    GameEntityType[GameEntityType.Empty][0],
    GameEntityType[GameEntityType.Gold][0],
    GameEntityType[GameEntityType.Player][0],
    GameEntityType[GameEntityType.Destination][0],
    GameEntityType[GameEntityType.Obsticle][0],
    "\n"
];