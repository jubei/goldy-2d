import { TemplateError } from '../Errors/errors'
import { WHITELIST } from './whitelist';
import { GameLevelStructure } from '../GameLevel/game-level';

export class Template {
    public layout: string;
    public breakpoint: number;

    constructor(level: GameLevelStructure) {
        this.breakpoint = level.breakpoint;
        this.layout = this.validate(level.layout);
     }

    validate(s: string) {
        s = s.trim();

        if (s.length === 0) {
            throw new Error(TemplateError.TemplateStringEmpty);
        }

        if (s.length % this.breakpoint != 0) {
            throw new Error(TemplateError.TemplateDimentionsError);
        }

        s.split('').every(c => WHITELIST.indexOf(c) >= 0);

        return s;
    }
}

export default Template;