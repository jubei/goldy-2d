import { GameBoard } from "./game-board";
import { Template } from "../Template/template";
import { GameEntity, GameEntityType } from "../GameEntity/game-entity";

export function gameBoardFromTemplate(t: Template) {
    let gameBoard = new GameBoard(t.breakpoint, t.layout.length/t.breakpoint);

    gameBoard.grid.forEach((c, index)=>{
        let line = Math.floor(index/t.breakpoint)

        switch (t.layout[index]) {
            case GameEntityType[GameEntityType.Player][0]:
                gameBoard.grid[index].storage = new GameEntity(GameEntityType.Player, line, index%t.breakpoint, gameBoard);
                break;
            case GameEntityType[GameEntityType.Gold][0]:
                gameBoard.grid[index].storage = new GameEntity(GameEntityType.Gold, line, index%t.breakpoint, gameBoard);
                break;
            case GameEntityType[GameEntityType.Obsticle][0]:
                gameBoard.grid[index].storage = new GameEntity(GameEntityType.Obsticle, line, index%t.breakpoint, gameBoard);
                break;
            case GameEntityType[GameEntityType.Destination][0]:
                gameBoard.grid[index].storage = new GameEntity(GameEntityType.Destination, line, index%t.breakpoint, gameBoard);
                break;
            default:
                break;
        }

    })

    return gameBoard;
}

export default gameBoardFromTemplate;