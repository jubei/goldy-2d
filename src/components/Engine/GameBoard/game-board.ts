import { Cell } from '../Cell/cell';
import { Board } from '../Board/board';
import { GameEntityType, GameEntity } from '../GameEntity/game-entity';
import { GameBoardError } from '../Errors/errors';

export class GameBoard extends Board {
    constructor (rows: number = 1, cols: number = rows) { 
        super(rows, cols);

        var k = 0;
        for (var m = 0; m < rows; m++) {
            for (var n = 0; n < cols; n++) {
                this.grid[k++].storage = new GameEntity(GameEntityType.Empty, m, n, this);
            }
        }
    }

    addEntity(e: GameEntity, random: boolean = false) {
        var emptySpots = this.searchByType(GameEntityType.Empty);

        if (emptySpots.length) {
            let randomEntity = emptySpots[Math.floor(Math.random()*emptySpots.length)];
            e.row = randomEntity.row;
            e.col = randomEntity.col;
            
            this.grid[this.getPos(randomEntity.col, randomEntity.row)].storage = e;
        } else {
            throw new Error(GameBoardError.OutOfSpace);
        }
    }

    searchByType(needle: GameEntityType) {
        var results : GameEntity[] = [];
        this.grid.forEach((cell, pos)=>{
            if (cell.storage.id === needle) {
                results.push(cell.storage);
            }
        });

        return results;
    }


    canStepOn(e: GameEntity, target: Cell) {
        if (!target) return false;

        if (target.storage.type === GameEntityType.Empty && e.type !== GameEntityType.Obsticle) {
            return true;
        }

        if (target.storage.type === GameEntityType.Destination 
            && e.type === GameEntityType.Gold) {
                target.setPocket(target.storage);
                return true;
        }


        if (target.storage.type === GameEntityType.Destination
            && e.type === GameEntityType.Player) {
            target.setPocket(target.storage);
            return true;
        }
            
        return false;
    }
      
    relocate(e : GameEntity, targetCell : Cell) {
        let backup = { i: e.row, j: e.col };

        e.row = targetCell.storage.row;
        e.col = targetCell.storage.col;
        targetCell.storage = e;

        if (this.grid[this.getPos(backup.i, backup.j)].getPocket()){
            this.grid[this.getPos(backup.i, backup.j)].storage = this.grid[this.getPos(backup.i, backup.j)].getPocket();
            this.grid[this.getPos(backup.i, backup.j)].setPocket(undefined);
        } else {
            this.grid[this.getPos(backup.i, backup.j)].storage = new GameEntity(GameEntityType.Empty, backup.i, backup.j, this);
        }
    }
}

export default GameBoard;