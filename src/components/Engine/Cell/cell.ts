export class Cell {
	private pocket : any = undefined;

	constructor(public storage: any = undefined, public enabled : boolean = false) { }

	getPocket() {
		return this.pocket;
	}

	setPocket(contents : any) {
		this.pocket = contents;
	}
}

export default Cell;