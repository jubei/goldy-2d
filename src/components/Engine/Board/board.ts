import Cell from '../Cell/cell';
import { GameBoardError } from '../Errors/errors';

export class Board {
  public grid : Cell[] = [];
    
  constructor(public rows: number = 1, public cols: number = 1) {
    if (rows <= 0 || cols <= 0) 
      throw new Error(GameBoardError.NaturalNumbers);

    for (let k = 0; k < this.size; k++) {
      this.grid.push(new Cell()); //  fills empty
    }
  }

  get size() {
    return this.rows*this.cols;
  }

  getPos(i : number, j : number) {
    if (i < 0) i = this.rows - Math.abs(i);
    if (j < 0) j = this.cols - Math.abs(j);

    if (i >=  this.rows) i = i - this.rows;
    if (j >=  this.cols) j = j - this.cols;

    let pos = this.cols * (i+1) - (this.cols - j);
    return pos;
  }
 
  getCooredinates(pos : number) {
    let coordinates  = {
      i: Math.floor(pos / this.rows),
      j: pos % this.rows
    }

    return coordinates;
  }
}  
  
export default Board;