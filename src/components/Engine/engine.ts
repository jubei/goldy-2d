import { GameLevel } from "./GameLevel/game-level";
import { GamePlayer } from "./GamePlayer/game-player";
import { GameBoard } from "./GameBoard/game-board";
import { GameEntityType, GameEntity } from "./GameEntity/game-entity";
import { GameError } from "./Errors/errors"
import { Cell } from "./Cell/cell";
import Template from "./Template/template";

export class Engine {
    board: GameBoard;
    player: GamePlayer;

    constructor(protected level : GameLevel) { this.load(level); }

    restart() {
        this.board = undefined;
        this.player = undefined;
        this.load(new GameLevel(new Template(this.level.template)));
    }

    load(level : GameLevel) {
        this.level = level;
        this.board = this.validate(level.board);
        this.player = new GamePlayer(this.board, this.board.searchByType(GameEntityType.Player)[0]);
    }

    checkWinner() {
        var status = true;

        this.board.grid.forEach(cell => {
            if (cell.storage.type === GameEntityType.Gold && cell.getPocket() === undefined) {
                status = false;
            }
        });

        return status;
    }

    progress() {
        var total = 0;

        this.board.grid.forEach(cell => {
            if (cell.storage.type === GameEntityType.Gold && cell.getPocket() !== undefined) {
                total++;
            }
        });
        return total;
    }

    // @TODO: Improve validation.
    validate(board: GameBoard) : GameBoard{
        if (!board.searchByType(GameEntityType.Destination).length) {
            throw Error(GameError.Unsolvable);
        }      

        if (!board.searchByType(GameEntityType.Player).length) {
            throw Error(GameError.PlayerMissing);
        } 
        
        return board;
    }
}


export default Engine;