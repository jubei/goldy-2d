import * as React from "react";

export function Message(props: any) {
  return (
    <div className="message">
      <h2>{props.data}</h2>
    </div>
  )
}