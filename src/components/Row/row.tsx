import * as React from "react";

import { Square } from "../Square/square";

export function Row(props: any) {
    const cols : any[] = [];

    for (let i = 0; i<props.cols; i++) {
        let value = props.squares[i];
        let key = "c"+i;

        cols.push(
            <Square onClick={props.onClick} row={props.row} col={i}  value={value}  key={key} />
        );
    }

  return (
        <div className="row">
            {cols}
        </div>
  );
}