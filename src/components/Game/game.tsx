import * as React from "react";

import { Board } from '../Board/board';
import { AudioEffect } from '../AudioEffect/audio-effect';
import { Message } from './../Message/message';
import './game.scss';

import { Engine } from "../Engine/engine";
import { GameLevel } from "../Engine/GameLevel/game-level";
import { GameEntityType } from "../Engine/GameEntity/game-entity";

const LEVELS =  require('../Engine/levels.json');

import { EMOJIS, gridToEmojiGrid } from "../Engine/emojis";

export interface GameProps { action: string; adjustBoardHeight: any; } // Expecting ArrowUp, ArrowLeft, ArrowRight or ArrowDown

export interface GameState {
    squares: any[];
    levelComplete: boolean;
    gameOver: boolean;
    level: number;
    goldies: number;
    progress: boolean;
}

export class Game extends React.Component<GameProps, GameState> {
    engine : Engine; // !important
    audioEffects: string[];

    constructor(props : any){
        super(props);

        this.engine = new Engine(new GameLevel(LEVELS[0]));

        this.state = {
            squares: gridToEmojiGrid(this.engine.board.grid),
            levelComplete: false,
            gameOver: false,
            level: 0,
            goldies: 0,
            progress: false
        };

        this.restartLevel = this.restartLevel.bind(this);
        this.onBoardNavigation = this.onBoardNavigation.bind(this);
    }

    private emojiIndex(c: string): any {
        return EMOJIS[c];
    }

    flushState() {
        let winnerStatus = this.engine.checkWinner();

        var newState : any  = {};
        
        
        // Level complete and was final
        if (winnerStatus && this.state.level+1 === LEVELS.length) {
            
            newState = {
                squares: gridToEmojiGrid(this.engine.board.grid),
                gameOver: true,
                levelComplete: true,
                level: this.state.level
            };
        } 

        // Level complete but not a final level
        if (winnerStatus && this.state.level+1 < LEVELS.length) {

            this.engine.load(new GameLevel(LEVELS[this.state.level+1]));

            var progress = this.engine.progress();
            this.engine.progress()
            newState = {
                squares: gridToEmojiGrid(this.engine.board.grid),
                gameOver: false,
                levelComplete: true,
                level: this.state.level+1,
            };
        } 
        

        // Level not complete, game continues
        if (!winnerStatus) {
            newState = {
                squares: gridToEmojiGrid(this.engine.board.grid),
                gameOver: false,
                levelComplete: false,
                level: this.state.level,
                goldies: this.engine.progress(),
                progress: this.engine.progress() > this.state.goldies
            };
        } 


        this.setState(newState);
    }

    componentWillReceiveProps(props : any) {
        
        if (props.action) {
            switch (props.action) {
                case "ArrowUp":
                     this.engine.player.up();
                    break;
                case "ArrowDown":
                    this.engine.player.down();
                    break;
                case "ArrowLeft":
                    this.engine.player.left();
                    break;
                case "ArrowRight":
                    this.engine.player.right();
                    break;
                default:
                    break;
            }

            this.flushState();
        }      
    }

    componentDidMount () {
        this.props.adjustBoardHeight();
    }

    restartLevel() {
        this.engine.restart();
        this.flushState();
    }

    /* For mobile phones mainly */
    onBoardNavigation(row : number, col : number){
        if (this.engine.player.entity.col === col) {
            if (row < this.engine.player.entity.row) {
                this.engine.player.up();
            }

            if (row > this.engine.player.entity.row) {
                this.engine.player.down();
            }
        }

        if (this.engine.player.entity.row === row) {
            if (col < this.engine.player.entity.col) {
                this.engine.player.left();
            }

            if (col > this.engine.player.entity.col) {
                this.engine.player.right();
            }
        }

        this.flushState();
    }

    render() {
        const current = this.state.squares;

        if (this.state.gameOver) {
            return (<div>
                <Message data="Thanks for playing! You did great 🦊" />
                <AudioEffect src="assets/270402__littlerobotsoundfactory__jingle-win-00.wav" id="ae-gameover" play={true} />
                </div>);
        } else {
            return (
                <div id="game" className="clearfix">
                    <header>
                        <h2>Goldy <small>LEVEL {this.state.level+1} / {LEVELS.length}</small></h2>  
                        <p className="status">Navigate board and deliver goods.</p>
                    </header>
                    <main>
                        <Board onClick={this.onBoardNavigation} rows={this.engine.board.rows} cols={this.engine.board.cols} squares={current} />
                        <div><button className="restart" onClick={this.restartLevel}>Restart</button></div>
                        <AudioEffect src="assets/blip.wav" id="ae-blip" play={this.state.progress} />
                        <AudioEffect src="assets/win.wav" id="ae-win" play={this.state.levelComplete} />
                             
                    </main>
                    {/* <footer></footer> */}
               </div>
            );
        }
    }
}